# Sidan tools

Verktyg för att göra sidan-livet lite enklare.

## parser

Python-modul för att hämta inlägg från sidan i JSON-format.

### USAGE

```Python
import sidan

posts = sidan.get()
```

## snotify

**S**idan **Notify**
![alt text](http://i.imgur.com/PvtWICa.png "Notification")
Desktop-notififications från sidan för ubuntu (`notify-send`)

## Setup (Ubuntu)
1. Clona repot till lämplig plats
2. Confa crontab att köra scriptet med ett rimligt intervall, typ 10 minuter:
```bash
$ crontab -e
```
```bash
10 * * * * env DISPLAY=:0 /usr/bin/python /en/lämplig/plats/snotify.py >/dev/null 2>&1
```