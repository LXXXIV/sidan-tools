#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sidan
from sidan import Post
import json
import sys
from os.path import dirname, abspath, isfile
from subprocess import call

DIFF_PATH = '/tmp/sidan.json'
ICON_PATH = '/'.join([dirname(abspath(__file__)), 'img/nix.gif'])
NOTIFICATION_CMD = '/usr/bin/notify-send'

def main():
	"""Compares current posts to previous posts, and sends a notification if there's any new"""
	posts = sidan.get(local_file=sys.argv)
	old_posts = load_fetch()
	if not old_posts:
		old_posts = posts
	new_posts = get_diff(old_posts, posts)
	if len(new_posts) < 4:
		for post in new_posts:
			notific = format_message(post)
			call([NOTIFICATION_CMD, "-i", ICON_PATH, notific[0], notific[1]])
	else:
		call([NOTIFICATION_CMD, "-i", ICON_PATH, '{} {}'.format(len(new_posts), 'nya inlägg på sidan')])
	dump_fetch(posts)

def format_message(post):
	"""Generates a notification message from a post"""
	message = post.content
	sender = post.sender
	units = post.units
	participants = post.participants
	notification_header = 'Nytt inlägg av {}'.format(sender)
	notification_message = message or ''
	notification_footer = '\n'
	if units:
		notification_footer = '{}{} {}'.format(notification_footer, units, 'enhet')
		if units > 1:
			notification_footer = '{}{}'.format(notification_footer, 'er')
	if participants:
		notification_footer = '{} {} {}'.format(notification_footer, 'med ', ', '.join(participants))
	if sender == 'arrsidan':
		notification_header = sender
	return (notification_header, notification_message + '\n' + notification_footer)

def get_diff(old_posts, posts):
	"""Compare current posts with previous posts"""
	diff_marker = old_posts[0]
	new_posts = []
	for post in posts:
		if post.equals(diff_marker):
			return new_posts
		new_posts.append(post)
	return new_posts

def dump_fetch(posts):
	"""Dumps current posts to a json file"""
	serialized = [post.toJSON() for post in posts]
	with open(DIFF_PATH, 'w') as outfile:
		json.dump(serialized, outfile)

def load_fetch():
	"""Loads stored posts into post-objects"""
	if not isfile(DIFF_PATH):
		return None
	with open(DIFF_PATH, 'r') as infile:
		old_posts = json.load(infile)
	return [Post(post['content'], post['units'], post['sender'], post['date'], post['participants']) for post in old_posts]

if __name__ == "__main__":
    main()