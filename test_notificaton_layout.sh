#!/bin/bash
set -e
python snotify.py orig
echo "Waiting 5 seconds before testing $1" # Due to notify-send's stupid decision to not allow variable delay...
sleep 5
python snotify.py $1