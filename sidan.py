import re
import requests
from lxml import html

# Blame #84
# Missing likes

def get(page=None, local_file=[]):
	"""Get python-representation of sidan"""
	if len(local_file) > 1:
		tree = load_local(local_file)
	else: 
		tree = load_sidan()
	posts = parse_html(tree)
	return posts

def load_local(local_file):
	"""Load local html-file, useful for debugging/testing"""
	path = 'test/{}'.format(local_file[1])
	with open(path, 'r') as infile:
		page = infile.read()
	return html.fromstring(page)

def load_sidan():
	"""Get content from sidan"""
	page = requests.get('http://chalmerslosers.com/read.jsp').content
	return html.fromstring(page)

def get_post_details(postSender):
	"""Get tuple of post date and participants"""
	if postSender.getnext().tag == 'a':
		details = postSender.getnext().tail
	else:
		details = postSender.tail
	details = details.replace('|', '')
	details = strip_linebreaks(details)
	participants = re.findall('#+[\d]+', details)
	date = details.split()[-1].split('(')[0]
	return (date, participants)

def strip_linebreaks(text):
	"""Removes windows-style linebreaks from string"""
	return text.replace('\r\n', '')
	
def parse_html(tree):
	"""Traverses the html tree and scrapes post data"""
	posts = tree.xpath('//div')
	postObjects = []
	for post in posts:
		postBeers = post.findall(".//div/a/img")
		postText = post.find(".//b")
		postSenderCandidates = post.findall(".//a")
		for potSend in postSenderCandidates:
			if potSend.get('href') == '#':
				postSender = potSend
				break
		postdetails = get_post_details(postSender)	
		if not postText == None:
			postObj = Post(strip_linebreaks(postText.text),
				len(postBeers),
				postSender.text,
				postdetails[0],
				postdetails[1])
			postObjects.append(postObj)
	return postObjects

class Post():
	"""Class defining a post on sidan"""

	def __init__(self, content, units, sender, date, participants):
		self.content = content
		self.units = units
		self.sender = sender
		self.date = date
		self.participants = participants

	def content(self):
		"""Get post content"""
		return self.content

	def units(self):
		"""Get post units"""
		return self.units

	def sender(self):
		"""Get post sender"""
		return self.sender

	def date(self):
		"""Get post date"""
		return self.date

	def participants(self):
		"""Get post participants"""
		return self.participants

	def toJSON(self):
		"""Returns object as dict, which is JSON serializable"""
		return {
			"content": self.content,
			"units": self.units,
			"sender": self.sender,
			"date": self.date,
			"participants": self.participants
		}

	def equals(self, post):
		"""Checks equality of self and other post based on content and date"""
		return self.content == post.content and self.date == post.date 